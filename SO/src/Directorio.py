from src.Classes.Instructions.CPU import CPU
from src.Classes.Instructions.IO import IO
from src.Classes.Kernel import Kernel
from src.Classes.HardDisk import HardDisk
from src.Classes import Clock
from src.Classes.Directorio.Executable import Executable
from src.Classes.Directorio.Folder import Folder
from src.Classes.Instructions.Instr import Instr
from pip import logger
class Directorio():

    def __init__(self, disk, folder, kernel):

        self._disk = disk
        self._folder = folder
        self.carpetaInicial = folder
        self._kernel = kernel

    def crearArchivo(self, direccion):

        carpetas = self.parseDirecion(direccion)
        carpetaIterable = carpetas
        ultimaCarpeta = carpetaIterable[(len(carpetaIterable)-1)]
        carpetaIterable.remove(ultimaCarpeta)
        realizarCreacion = 1
        self.Home()
        for carpeta in carpetaIterable:
            if self.existeCarpeta(carpeta) :
                self.entrarEnCarpeta(carpeta)
            else:
                realizarCreacion = 0
                print("No se pudo crear la carpeta en esa direccion")
                break

        if not self.estaRepetido(ultimaCarpeta) and realizarCreacion:
            nuevaCarpeta = Folder([], ultimaCarpeta)
            self._folder.files.append(nuevaCarpeta)
        self.Home()
        
    def crearEjecutable(self, direccion,disk):

        carpetas = self.parseDirecion(direccion)
        carpetaIterable = carpetas
        ejecutableACrear = carpetaIterable[(len(carpetaIterable)-1)]
        ejecutable = carpetaIterable[(len(carpetaIterable)-1)]
        carpetaIterable.remove(ejecutable)
        realizarCreacion = 1
        self.Home()
        for carpeta in carpetaIterable:
            if self.existeCarpeta(self.sacarPrimerElemento(carpeta)):
                self.entrarEnCarpeta(self.sacarPrimerElemento(carpeta))

            else:
                realizarCreacion = 0
                print("No se pudo crear el ejecutable en esa direccion")
                break

        if not self.estaRepetido(ejecutableACrear) and realizarCreacion:

            nuevoEjecutable = Executable(ejecutable,disk.read(self.sacarPrimerElemento(ejecutable)))
            self._folder.files.append(nuevoEjecutable)
        self.Home()


    def estaRepetido(self,elementoNuevo):
        repeticion = 0
        for carpeta in self._folder.files:
            if elementoNuevo == carpeta.name:
                repeticion = 1
        return repeticion

    def existeCarpeta(self,nombre):
        name = self.buscar(nombre)
        if name != []:
            return 1
        else:
            return 0

    def verProgramasEnLaCarpetaActual(self):
        carpetas = ""
        for folder in self._folder.files:
            carpetas = carpetas + " " +folder.printColor()

        print(carpetas)

    def EjecutarPrograma(self, programa):
        random = 1
        self._kernel.exec(programa, random)


    def Home(self):
        self._folder = self.carpetaInicial

    def entrarEnCarpeta(self, nombre):

        if (self.buscar(nombre)) != [] and (self.buscar(nombre)).notExe():
            self._folder = self.buscar(nombre)
        else:
            print("Error: Carpeta no encontrada")


    def buscar(self, nombre):
        carpeta = []
        for file in self._folder.files:
            if file.name == nombre:
                carpeta = file
        return carpeta

    def parseDirecion(self, direccion):
        carpetas = []
        indice = 0
        carpetas.append("")
        for string in direccion:
            if string != "/":
               carpetas[indice] = carpetas[indice] + string
            else:
                carpetas.append("")
                indice = indice +1
        return carpetas

    def sacarPrimerElemento(self,string):
        nuevoString = ""
        for i in string:
            if i != " ":
                nuevoString = nuevoString+ i

        return nuevoString




    def cantidadDeSeparadores(self, direccion):
        count = 0
        for string in direccion:
            if string == "/":
                count = count+1
        return count




