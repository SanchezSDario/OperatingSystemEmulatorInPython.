#!/usr/bin/env python

import logging
import threading

from Classes.Instructions.CPU import CPU
from Classes.Instructions.IO import IO

from src.Classes.Directorio.Executable import Executable
from src.Classes.Directorio.Folder import Folder
from src.Classes.LFU import LFU
from src.Classes.LRU import LRU
from src.Consola import Consola
from src.Directorio import Directorio

from src.Factory import Factory

if __name__ == '__main__':

# Configure Logger
    handler = logging.FileHandler("SistemaOperativo.log")
    logger = logging.getLogger()
    #handler = logging.StreamHandler()
    formatter = logging.Formatter('%(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    logger.info('Starting emulator')


#Factory impl
    frameSize = 2 #For paging
    selectVictimAlgoritm = LRU() #for demand
    prototype = Factory()
    #prototype.withMemoryContiguous(24)
    prototype.withMemoryPaging(16, frameSize)
    #prototype.withMemoryDemandPaging(8, frameSize, selectVictimAlgoritm)
    prototype.withSchedulingFCFS()
    #prototype.withSchedulingNonPreemptivePriority(5)
    #prototype.withSchedulingPreemptivePriority(5)
    #prototype.withSchedulingRR(2)
    prototype.withIoDeviceController()\
             .withDefaultInterruptions() \
             .withDevice("PS4", 5) \
             .withDevice("FloppyDisk", 2) \
             .withDevice("GameBoy", 3) \
             .withClock()

    clock = prototype.clock
    kernel = prototype.kernel
    disk = prototype.disk

    # write with contiguous
    # disk.write("Fifa18.exe", [CPU(2), IO("PS4"), CPU(2)])
    # disk.write("MonkeyIsland.exe", [CPU(4), IO("FloppyDisk")])
    # disk.write("Smite.exe", [CPU(6)])
    # disk.write("Metroid.exe", [IO("GameBoy"), CPU(3)])

    # write with paging
    disk.write("Fifa18.exe", [CPU(2), IO("PS4"), CPU(2)], frameSize)
    disk.write("MonkeyIsland.exe", [CPU(4), IO("FloppyDisk")], frameSize)
    disk.write("Smite.exe", [CPU(6)], frameSize)
    disk.write("Metroid.exe", [IO("GameBoy"), CPU(3)], frameSize)
    disk.write("programa2", [CPU(6)], frameSize)

    def startKernel():
        clock.ticks()
        kernel.exec("Fifa18.exe",2)  # Second parameter refers the priority, doesn't bother other schedulings if it's given.
        clock.ticks(1)
        kernel.exec("MonkeyIsland.exe",1)  # Second parameter refers the priority, doesn't bother other schedulings if it's given.
        clock.ticks(4)
        kernel.exec("Smite.exe", 4)
        clock.ticks(3)
        kernel.exec("Metroid.exe", 3)
        clock.ticks(17)
        clock.ticks(9999)

    def startTerminal():
        system = Folder([Executable("programa2", [CPU(2)])] , "System")
        carpetaC = Folder([Executable("programa", [CPU(1), ]),system],"c")
        miPC = Folder([carpetaC],"MIPC")
        directorio = Directorio(disk, miPC, kernel)
        terminal = Consola(directorio,disk, kernel)
        terminal.run()

#Running console


    t_console = threading.Thread(target=startTerminal)
    t_kernel = threading.Thread(target=startKernel)




    #start threads
    t_console.start()
    t_kernel.start()


    # join threads (syncronize ending)

    t_console.join()
    t_kernel.join()




