
import sys

from pip import logger

from src.Classes.Dispatcher.MMU import MMUPagingBD
from src.Classes.LRU import LRU
from src.Classes.Loader import LoaderPagingBD

sys.path.append('../src')


from Classes.Clock import Clock
from Classes.Dispatcher.Cpu import Cpu
from Classes.Dispatcher.Dispatcher import Dispatcher, DispatcherPaging
from Classes.Dispatcher.MMU import MMU, MMUPaging
from Classes.DummyTimer import DummyTimer
from Classes.HardDisk import HardDisk, HardDiskPaging
from Classes.IODeviceController.IODevice import IODevice
from Classes.IODeviceController.IODeviceController import IODeviceController
from Classes.IRQ import InterruptionTypes
from Classes.InterruptVector.InterruptVector import InterruptVector
from Classes.InterruptVector.InterruptionHandlerIOIN import InterruptionHandlerIOIN
from Classes.InterruptVector.InterruptionHandlerIOOUT import InterruptionHandlerIOOUT
from Classes.InterruptVector.InterruptionHandlerKill import InterruptionHandlerKill
from Classes.InterruptVector.InterruptionHandlerNew import InterruptionHandlerNew
from Classes.InterruptVector.InterruptionHandlerPageFault import InterruptionHandlerPageFault
from Classes.InterruptVector.InterruptionHandlerTimeOut import InterruptionHandlerTimeOut
from Classes.Kernel import Kernel
from Classes.Loader import Loader, LoaderPaging
from Classes.Memory import Memory
from Classes.MemoryManager import MemoryManager
from Classes.Scheduler.FCFSScheduling import FCFSScheduling
from Classes.Scheduler.PriorityExp import PriorityExp
from Classes.Scheduler.PriorityNoExp import PriorityNoExp
from Classes.Timer import Timer


class Factory:

    def __init__(self):
        self._kernel = Kernel()
        self._clock = Clock()

    @property
    def kernel(self):
        return self._kernel

    @property
    def clock(self):
        return self._clock

    @property
    def disk(self):
        return self.kernel.loader._hardDisk

    def withMemoryContiguous(self, capacity):
        disk = HardDisk()
        memory = Memory(capacity)
        mmu = MMU(memory)
        cpu = Cpu(mmu)
        programLoader = Loader(disk, memory)
        dispatcher = Dispatcher(cpu, mmu)
        self.kernel.loader = programLoader
        self.kernel.dispatcher = dispatcher
        return self

    def withMemoryPaging(self, capacity, frameSize):
        disk = HardDiskPaging()
        memory = Memory(capacity)
        mmu = MMUPaging(memory, frameSize)
        cpu = Cpu(mmu)
        memoryManager = MemoryManager(frameSize, memory)
        dispatcher = DispatcherPaging(cpu, mmu, memoryManager)
        programLoader = LoaderPaging(disk, memory, memoryManager)
        self.kernel.loader = programLoader
        self.kernel.dispatcher = dispatcher
        return self

    def withMemoryDemandPaging(self, capacity, frameSize, algoritm):
        disk = HardDiskPaging()
        memory = Memory(capacity)
        mmu = MMUPagingBD(memory, frameSize)
        cpu = Cpu(mmu)
        memoryManager = MemoryManager(frameSize, memory)
        algoritm.pageTable = memoryManager.pageTable
        memoryManager.algoritmo = algoritm
        dispatcher = DispatcherPaging(cpu, mmu, memoryManager)
        programLoader = LoaderPagingBD(disk, memory, memoryManager)
        mmu.memoryManager = memoryManager
        mmu.pageTable = memoryManager.pageTable
        mmu.kernel = self.kernel
        self.kernel.loader = programLoader
        self.kernel.dispatcher = dispatcher
        return self

    def withSchedulingFCFS(self):
        timer = DummyTimer()
        scheduler = FCFSScheduling()
        self.kernel.scheduler = scheduler
        self.clock.timer = timer
        self.kernel.dispatcher.timer = timer
        return self

    def withSchedulingPreemptivePriority(self, priorityLevels):
        timer = DummyTimer()
        scheduler = PriorityExp(priorityLevels)
        self.kernel.scheduler = scheduler
        self.clock.timer = timer
        self.kernel.dispatcher.timer = timer
        return self

    def withSchedulingNonPreemptivePriority(self, priorityLevels):
        timer = DummyTimer()
        scheduler = PriorityNoExp(priorityLevels)
        self.kernel.scheduler = scheduler
        self.clock.timer = timer
        self.kernel.dispatcher.timer = timer
        return self

    def withSchedulingRR(self, quantum):
        timer = Timer(quantum)
        scheduler = FCFSScheduling()
        self.kernel.scheduler = scheduler
        self.clock.timer = timer
        self.kernel.dispatcher.timer = timer
        return self

    def withDefaultInterruptions(self):
        interruptVector = InterruptVector()
        interruptVector.register(InterruptionTypes.NEW, InterruptionHandlerNew(self.kernel))
        interruptVector.register(InterruptionTypes.KILL, InterruptionHandlerKill(self.kernel))
        interruptVector.register(InterruptionTypes.IOIN, InterruptionHandlerIOIN(self.kernel))
        interruptVector.register(InterruptionTypes.IOOUT, InterruptionHandlerIOOUT(self.kernel))
        interruptVector.register(InterruptionTypes.TimeOut, InterruptionHandlerTimeOut(self.kernel))
        interruptVector.register(InterruptionTypes.PAGEFAULT, InterruptionHandlerPageFault(self.kernel))
        self.kernel.interruptVector = interruptVector
        self.kernel.dispatcher.cpu.interruptVector = interruptVector
        self.clock.timer.interruptVector = interruptVector
        return self

    def withIoDeviceController(self):
        ioDeviceController = IODeviceController()
        self.kernel.ioDeviceController = ioDeviceController
        return self

    def withDevice(self, deviceName, time):
        device = IODevice(self.kernel.ioDeviceController, deviceName, time, self.kernel.interruptVector)
        self.kernel.ioDeviceController.addDevice(device)
        return self

    def withClock(self):
        self.clock.cpu = self.kernel.dispatcher.cpu
        self.clock.ioDeviceList = self.kernel.ioDeviceController.devices()
        return self