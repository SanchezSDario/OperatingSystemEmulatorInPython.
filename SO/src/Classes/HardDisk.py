from Classes.Instructions.EXIT import EXIT

class HardDisk:
    def __init__(self):
        self._content = {}
        self._puntero = []

    def write(self, programName, instructions):
        self.content[programName] = instructions
        self.puntero.append(programName)

    def read(self, programName):
        return self.content[programName]

    @property
    def content (self):
        return self._content

    @property
    def puntero(self):
        return self._puntero
    
    
    
class HardDiskPaging:
    def __init__(self):
        self._content = {}
        self._puntero = []

    def write(self, programName, ins, frameSize):
        instructions = self.expand(ins)
        offset = len(instructions) % frameSize
        cantPages= len(instructions) // frameSize + offset
        programPages = []
        for n in range (0, cantPages):
            pagina = []
            for n in range (0,frameSize):
                if len(instructions) != 0 :
                    pagina.append(instructions.pop(0))
            programPages.append(pagina)
        self.content[programName] = programPages  
        
        self._puntero.append(programName)
        
    def expand(self, instructions):
        expanded = []
        for instr in instructions:
            expanded.extend(instr.expand())
        if not expanded[-1].isExit():
            expanded.append(EXIT(0))

        return expanded  
                
            
    def read(self, programName):
        return self.content[programName]

    @property
    def content (self):
        return self._content
