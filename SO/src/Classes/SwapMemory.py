from collections import deque

from pip import logger
from tabulate import tabulate


class SwapMemory:

    def __init__(self, size):
        self._memoryArray = deque(maxlen=size)
        self._size = size
        for i in range(0, size):
            self._memoryArray.append(None)

    def get(self, addr):
        return self._memoryArray[addr]

    def put(self, addr, value):
        self._memoryArray[addr] = value

    def fetch(self, addr):
        logger.info("-swapeando: {valor}".format(valor=self._memoryArray[addr]))
        return self._memoryArray[addr]

    def __repr__(self):
        return tabulate(enumerate(self._memoryArray), tablefmt='psql')

    @property
    def getMemory(self):
        return self._memoryArray

    @property
    def size(self):
        return self._size