class Folder():
    def __init__(self, files, name):
        self._files = files
        self._name = name

    def notExe(self):
        return 1

    def printColor(self):
        return chr(27)+"[9;94m " + self._name

    @property
    def name(self):
        return self._name

    @property
    def files(self):
        return self._files

