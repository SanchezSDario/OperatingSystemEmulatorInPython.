class Executable():
    def __init__(self, name, instructions):
        self._name = name
        self._instructions = instructions

    def notExe(self):
        return 0
    def printColor(self):
        return chr(27)+"[9;91m " + self._name
    @property
    def name(self):
        return self._name

    @property
    def instructions(self):
        return self._instructions

