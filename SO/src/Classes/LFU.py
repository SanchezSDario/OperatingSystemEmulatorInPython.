from pip import logger


class LFU:
    def __init__(self):
        self._pageTG = None

    def selectV(self):
        usos = 1000
        idRow = None
        pidV = None
        for pid in self._pageTG.table.keys():
            pageTL = self._pageTG.getPageTable(pid)

            if self.tieneAlgo(pageTL):
                idLowRow = self.getLowRow(pageTL, usos)
                row = pageTL.getRow(idLowRow)
                if row.usos < usos:
                    usos = row.usos
                    pidV = pid
                    idRow = idLowRow
                    logger.info("pid: {num} ".format(num=pidV))
                    logger.info("idRowCompared: {idRowCompared}".format(idRowCompared=idRow))
        return pidV, idRow

    def getLowRow(self, pagTL, usos):
        pageNumber = 0
        # row = None
        usos = usos
        for page in range(0, len(pagTL.table)):
            row = pagTL.getRow(page)
            if ((row.validB == 1) and (row.usos < usos)):
                usos = row.usos
                pageNumber = page
                logger.info("RowId= {r}, usosIterando: {num}, usosStatic: {nun} ".format
                            (num=pagTL.getRow(page).usos, nun=usos, r=pageNumber))
        return pageNumber

    def tieneAlgo(self, pt):
        tiene = []
        for i in range(0, len(pt.table)):
            if (pt.table[i].validB == 1):
                tiene.append(True)
        return tiene.count(True) > 0

    @property
    def pageTable(self):
        return self._pageTG

    @pageTable.setter
    def pageTable(self, ptg):
        self._pageTG = ptg

