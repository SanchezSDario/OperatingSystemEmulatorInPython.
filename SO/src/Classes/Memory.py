from tabulate import tabulate
from _collections import deque
from pip import logger


class Memory:
    def __init__(self, size):
        self._memoryArray = deque(maxlen=size)
        self._size = size
        for i in range(0, size):
            self._memoryArray.append(None)

    def get(self, addr):
        return self._memoryArray[addr]

    def put(self, addr, value):
        self._memoryArray[addr] = value
        logger.info("\n-cargadoEnDir: {num}, ins: {valor}, \n{memory}\n".format(
            num=addr, valor=value, memory = self))

    def fetch(self, addr):
        return self._memoryArray[addr]

    def __repr__(self):
        return tabulate(enumerate(self._memoryArray), tablefmt='psql')

    @property
    def getMemory(self):
        return self._memoryArray

    @property
    def size(self):
        return self._size
