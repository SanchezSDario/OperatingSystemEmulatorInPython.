from enum import Enum


class InterruptionTypes(Enum):
    NEW = 1
    KILL = 2
    IOIN = 3
    IOOUT = 4
    TimeOut = 5
    PAGEFAULT = 6


class InterruptionParameters:
    NEW_PROGRAM_NAME = "PROGRAM_NAME"
    IO_DEVICE = "DEVICE_NAME"
    IO_OUT_PCB = "PCB"
    PCB_PRIORITY = "PRIORITY"
    PAGE_FAULT = "PAGE"


class IRQ:
    def __init__(self, interruptionType):
        self._type = interruptionType
        self._parameters = {}

    @property
    def type(self):
        return self._type

    def set(self, parameterName, parameterValue):
        self._parameters[parameterName] = parameterValue

    def get(self, parameterName):
        return self._parameters[parameterName]
