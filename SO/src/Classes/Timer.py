
import sys
sys.path.append('../src')

from Classes.IRQ import IRQ
from Classes.IRQ import InterruptionTypes, InterruptionParameters
from pip import logger


class Timer:
    def __init__(self, quantum, interruptVector=None):
        self._quantum = quantum
        self._interruptVector = interruptVector
        self._tickCount = 0

    def tick(self):
        self._tickCount += 1
        if(self._tickCount > self._quantum):
            irq = IRQ(InterruptionTypes.TimeOut)
            self._interruptVector.handle(irq)

    def reset(self):
        self._tickCount = 0

    @property
    def tickCount(self):
        return self._tickCount

    @property
    def quantum(self):
        return self._quantum

    @property
    def interruptVector(self):
        return self._interruptVector

    @interruptVector.setter
    def interruptVector(self, ivV):
        self._interruptVector = ivV