import datetime

from pip import logger

from Classes.Instructions.EXIT import EXIT

from src.Classes.PageTable import PageTableBD, PageTableRow


class Loader:
    def __init__(self, hardDisk, memory):
        self._hardDisk = hardDisk
        self._memory = memory
        self._pointer = 0

    def loadInstructions(self, instructions):
        self._instructions = self.expand(instructions)
        dirBase = self._pointer
        for inst in self._instructions:
            self._memory.put(self._pointer, inst)
            self._pointer += 1
        return dirBase

    def load(self, pcb):
        instructions = self._hardDisk.read(pcb.path)
        dirBase = self.loadInstructions(instructions)
        pcb.baseDir = dirBase
        return True

    def expand(self, instructions):
        expanded = []
        for instr in instructions:
            expanded.extend(instr.expand())
        if not expanded[-1].isExit():
            expanded.append(EXIT(0))
        return expanded

    @property
    def memory(self):
        return self._memory

class LoaderPaging:
    def __init__(self, hardDisk, memory, memoryManager):
        self._hardDisk = hardDisk
        self._memoryManager = memoryManager
        self._memory = memory

    @property
    def hardDisk(self):
        return self._hardDisk

    @property
    def memoryManager(self):
        return self._memoryManager

    @property
    def memory(self):
        return self._memory

    def load(self, pcb):
        pages = self._hardDisk.read(pcb.path)
        cantPages = len(pages)
        if (cantPages <= len(self.memoryManager.freeFrames)):
            pageTable = PageTableBD()
            for i in range(0, cantPages):
                pageTR = PageTableRow()
                pageTR.validB = 1
                pageTR.timeStamp = datetime.datetime.now().timestamp()
                pageTR.frame = self.memoryManager.getFreeFrame()
                pageTable.table.append(pageTR)
                self.loadPage(pages[i], pageTR.frame)
            self.memoryManager.pageTable.addPageTable(pcb.pid, pageTable)
            return True

        else:
            return False
        
    def loadPage(self, page, frame):
        frameBaseDir = frame * self.memoryManager.frameSize
        dir = frameBaseDir
        for instruction in page:
            self.memory.put(dir, instruction)
            dir += 1


class LoaderPagingBD:

    def __init__(self, hardDisk, memory, memoryManager):
        self._hardDisk = hardDisk
        self._memoryManager = memoryManager
        self._memory = memory

    @property
    def hardDisk(self):
        return self._hardDisk

    @property
    def memoryManager(self):
        return self._memoryManager

    @property
    def memory(self):
        return self._memory

    def load(self, pcb):
        pages = self._hardDisk.read(pcb.path)
        cantPages = len(pages)
        pageTable = PageTableBD()
        for i in range (0, cantPages):
            pageTable.table.append(PageTableRow())
        self.memoryManager.pageTable.addPageTable(pcb.pid, pageTable)
        return True

    def loadPage(self, pageToLoad, frame):
        frameBaseDir = frame * self.memoryManager.frameSize
        dir= frameBaseDir
        for instruction in pageToLoad:
            self.memory.put(dir, instruction)
            dir += 1
