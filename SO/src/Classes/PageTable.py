import datetime

from pip import logger
from tabulate import tabulate


class PageTableGlobal:
    def __init__(self):
        self._table = {}

    @property
    def table(self):
        return self._table

    def addPageTable(self, pid, pageT):
        self._table[pid] = pageT

    def getPageTable(self, pid):
        return self._table[pid]

    def updatePage(self, pid, pageT):
        self.table[pid] = pageT

    def __repr__(self):
        return tabulate(enumerate(self._table), tablefmt='psql')

class PageTableBD:
    def __init__(self):
        self._table = []

    @property
    def table(self):
        return self._table

    def getRow(self, page):
        return self._table[page]

    def putRow(self, page, pageRow):
        self.table[page] = pageRow

    def __repr__(self):
        return tabulate(enumerate(self._table), tablefmt='psql')


class PageTableRow:
    def __init__(self):
        self._frame = None
        self._usos = 0
        self._validB = 0
        self._dirty = 0
        self._timeStamp = datetime.datetime.now().timestamp()
        self._swap = None
        self._secondChance = 0

    @property
    def validB(self):
        return self._validB

    @property
    def dirty(self):
        return self._dirty

    @property
    def frame(self):
        return self._frame

    @property
    def timeStamp(self):
        return self._timeStamp

    @property
    def swap(self):
        return self._swap

    @property
    def secondChance(self):
        return self._secondChance

    @frame.setter
    def frame(self, frame):
        self._frame = frame

    @validB.setter
    def validB(self, v):
        self._validB = v

    @timeStamp.setter
    def timeStamp(self, t):
        self._timeStamp = t

    @property
    def usos(self):
        return self._usos

    @usos.setter
    def usos(self, amount):
        self._usos = amount

    def __repr__(self):
        return "{frame}".format(frame=self._frame)