from enum import Enum


class PCBState(Enum):
    NEW = 0
    READY = 1
    RUNNING = 2
    WAITING = 3
    TERMINATED = 4


class PCB:
    def __init__(self, pid, baseDir, path, state):
        self._pid = pid
        self._baseDir = baseDir
        self._path = path
        self._pc = 0
        self._state = state
        self._priority = 0

    @property
    def priority(self):
        return self._priority

    @property
    def pid(self):
        return self._pid

    @property
    def baseDir(self):
        return self._baseDir

    @property
    def path(self):
        return self._path

    @property
    def pc(self):
        return self._pc

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state

    @baseDir.setter
    def baseDir(self, baseDir):
        self._baseDir = baseDir

    def setPc(self, pc):
        self._pc = pc

    @pid.setter
    def pid(self, pid):
        self._pid = pid

    @path.setter
    def path(self, path):
        self._path = path

    @priority.setter
    def priority(self, prio):
        self._priority = prio

    def __repr__(self):
        return "\nPCBpid : {pid} - PCBpath : {path} - PCBstate : {state} \n".format(
            pid = self.pid, path=self.path, baseDir=self.baseDir, state = self.state)
