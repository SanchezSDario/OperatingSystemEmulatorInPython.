from locale import currency

import datetime

from pip import logger


class LRU:
    def __init__(self):
        self._pageTG = None

    def selectV(self):
        timeStamp = datetime.datetime.now().timestamp()
        idRow = None
        pidV = None
        for pid in self._pageTG.table.keys():
            pageTL = self._pageTG.getPageTable(pid)

            if self.tieneAlgo(pageTL):
                idLowRow = self.getLowRow(pageTL, timeStamp)
                row = pageTL.getRow(idLowRow)
                if row.timeStamp < timeStamp:
                    timeStamp = row.timeStamp
                    pidV = pid
                    idRow = idLowRow
                    logger.info("pid: {num} ".format(num=pidV))
                    logger.info("idRowCompared: {idRowCompared}".format(idRowCompared=idRow))
        return pidV, idRow

    def getLowRow(self, pagTL, timeStamp):
        pageNumber = 0
        #row = None
        time = timeStamp
        for page in range(0, len(pagTL.table)):
            row = pagTL.getRow(page)
            if ((row.validB == 1) and (row.timeStamp < time)):
                time = row.timeStamp
                pageNumber = page
                logger.info("RowId= {r}, TSiterando: {num}, TSstatic: {nun} ".format
                            (num=pagTL.getRow(page).timeStamp, nun=timeStamp, r= pageNumber))
        return pageNumber

    def tieneAlgo(self, pt):
        tiene = []
        for i in range(0, len(pt.table)):
            if(pt.table[i].validB == 1):
                tiene.append(True)
        return tiene.count(True) > 0

    @property
    def pageTable(self):
        return self._pageTG

    @pageTable.setter
    def pageTable(self, ptg):
        self._pageTG = ptg

