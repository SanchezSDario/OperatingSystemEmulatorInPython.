from . import Instr
class EXIT(Instr.Instr):

    def isExit(self):
        return True

    def __repr__(self):
        return "EXIT"