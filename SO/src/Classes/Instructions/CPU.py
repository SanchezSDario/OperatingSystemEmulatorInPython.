from . import Instr

class CPU(Instr.Instr):

    def isCpu(self):
        return True

    def __repr__(self):
        if self._count:
            return "CPU({count})".format(count=self._count)
        else:
            return "CPU"