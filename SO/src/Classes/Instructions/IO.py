from . import Instr

class IO(Instr.Instr):

    def __init__(self, count):
        self._count = count

    def isIo(self):
        return True

    def isExit(self):
        return False

    def __repr__(self):
        if self._count:
            return "IO({count})".format(count=self._count)
        else:
            return "IO"

    @property
    def count(self):
        return self._count