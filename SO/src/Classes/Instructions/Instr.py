class Instr():

    def __init__(self, count=1):
        self._count = count

    def isExit(self):
        return False

    def isIo(self):
        return False

    def isCpu(self):
        return False

    @property
    def count(self):
        return self._count

    def expand(self):
        expanded = []
        if(self.isIo()):
            return [self]
        else:
            for _ in range(self._count):
                expanded.append(self.__class__(0))
            return expanded

