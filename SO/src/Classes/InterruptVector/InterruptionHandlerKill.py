from src.Classes.PCB import PCBState
from pip import logger


class InterruptionHandlerKill:

    def __init__(self, kernel):
        self._pcbTable   = kernel.pcbTable
        self._dispatcher = kernel.dispatcher
        self._scheduler  = kernel.scheduler
        self._loader = kernel.loader

    def execute(self, irq):
        pcb = self.pcbTable.runningPCB
        pcb.state = PCBState.TERMINATED
        self.dispatcher.save(pcb)
        #agregado ultimo
        
        self.pcbTable.setRunningPCB(None)
        
        #al terminar un proceso me fijo si quedo uno espera, y si se puede cargar
        if len(self.scheduler.waitingQ) > 0 :
            pcbWaiting = self.scheduler.getFromWaitingQ()
            if self.loader.load(pcbWaiting):
                self.scheduler.add(pcbWaiting)
                
        #self.pcbTable.remove(pcb.pid)  ##no me deja remover el pid de la pcbTable
        logger.info("{pcbTable}".format(pcbTable = self.pcbTable))
        logger.info("\nScheduler after KILL \n{scheduler}\n".format(scheduler = self.scheduler))
        #self.pcbTable.setRunningPCB(None)
        if not self.scheduler.isEmpty():
            next = self.scheduler.getNext()
            next.state = PCBState.RUNNING
            self.dispatcher.load(next)
            self.pcbTable.setRunningPCB(next)

    @property
    def pcbTable(self):
        return self._pcbTable

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def loader(self):
        return self._loader
