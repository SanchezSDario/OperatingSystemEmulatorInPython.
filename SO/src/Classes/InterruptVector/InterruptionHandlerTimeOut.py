from src.Classes.PCB import PCBState


class InterruptionHandlerTimeOut:
    def __init__(self, kernel):
        self.value = 0
        self._pcbTable = kernel.pcbTable
        self._scheduler = kernel.scheduler
        self._dispatcher = kernel.dispatcher

    def execute(self, irq):
        if(not self._pcbTable.runningPCB == None):
           runningPCB = self._pcbTable.runningPCB
           self._dispatcher.save(runningPCB)
           if(self._scheduler.isEmpty()):
               self._dispatcher.load(runningPCB)
           else:
               runningPCB.state = PCBState.WAITING
               self._scheduler.add(runningPCB)
               next = self._scheduler.getNext()
               next.state = PCBState.RUNNING
               self._pcbTable.setRunningPCB(next)
               self._dispatcher.load(next)




