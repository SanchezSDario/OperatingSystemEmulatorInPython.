from pip import logger

from src.Classes.IRQ import InterruptionParameters
from src.Classes.PCB import PCBState


class InterruptionHandlerIOIN:
    def __init__(self, kernel):
        self._pcbTable = kernel.pcbTable
        self._dispatcher = kernel.dispatcher
        self._scheduler = kernel.scheduler
        self._ioDeviceController = kernel.ioDeviceController

    def execute (self, irq):
        device = self._ioDeviceController.get(irq.get(InterruptionParameters.IO_DEVICE))
        runningPCB = self.pcbTable.runningPCB
        
        self.dispatcher.save(runningPCB)
        if not self.scheduler.isEmpty():
            runningPCB.state = PCBState.WAITING
            device.waitingQ.append(runningPCB)
            next = self.scheduler.getNext()
            next.state = PCBState.RUNNING
            self.pcbTable.setRunningPCB(next)
            self.dispatcher.load(next)
        else:
            self.pcbTable.setRunningPCB(runningPCB)
            self.dispatcher.load(runningPCB)

    @property
    def pcbTable(self):
        return self._pcbTable

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def scheduler(self):
        return self._scheduler
