from Classes.PCB import PCB

from Classes.IRQ import InterruptionParameters
from pip import logger

from src.Classes.PCB import PCBState


class InterruptionHandlerNew:
    def __init__(self, kernel):
        self._pcbTable = kernel.pcbTable
        self._scheduler = kernel.scheduler
        self._dispatcher = kernel.dispatcher
        self._loader = kernel.loader

    def execute(self, irq):
        programName = irq.get(InterruptionParameters.NEW_PROGRAM_NAME)
        pcb = PCB(0, 0, "", "")
        pcb.path = programName
        pcb.priority = irq.get(InterruptionParameters.PCB_PRIORITY)
        pcb.pid = self.pcbTable.getNewPID()
            
        #SI SE PUDO CARGAR EL PCB, ENTONCES
        if self.loader.load(pcb):
            self.pcbTable.add(pcb)
            logger.info("\n NEW \n --Loaded in readyQ-- pid: {pid}".format(pid=pcb.pid))
        
            runningPCB = self.pcbTable.runningPCB
            if (runningPCB != None):
                if self.scheduler.mustExpropiate(runningPCB, pcb):
                    pcbExpropiado = runningPCB
                    pcbExpropiado.state = PCBState.READY
                    self.dispatcher.save(pcbExpropiado)
                    self.scheduler.add(pcbExpropiado)
                    pcb.state = PCBState.RUNNING
                    self.dispatcher.load(pcb)
                    self.pcbTable.setRunningPCB(pcb)
                else:
                    pcb.state = PCBState.READY
                    self.scheduler.add(pcb)
            else:
                pcb.state = PCBState.RUNNING
                self.dispatcher.load(pcb)
                self.pcbTable.setRunningPCB(pcb)
                
        #SINO LO AGREGO A LA WAITING Q DEL SCHEDULER
        else:
            logger.info("\n NEW \n --Saved in waiting Q-- {sa}".format(sa = pcb))
            self.scheduler.addToWaitingQ(pcb)
            
        logger.info("{pcbTable}".format(pcbTable = self.pcbTable))
        logger.info("Scheduler after NEW : \n{scheduler}\n".format(scheduler = self.scheduler))

    @property
    def pcbTable(self):
        return self._pcbTable

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def loader(self):
        return self._loader

