from pip import logger

from Classes.IRQ import InterruptionParameters

from src.Classes.PCB import PCBState


class InterruptionHandlerIOOUT:

    def __init__(self, kernel):
        self._scheduler = kernel.scheduler
        self._dispatcher = kernel.dispatcher
        self._pcbTable = kernel.pcbTable

    def execute(self, irq):
        pcb = irq.get(InterruptionParameters.IO_OUT_PCB)
        runningPCB = self._pcbTable.runningPCB
        if (runningPCB != None):
            if self._scheduler.mustExpropiate(runningPCB, pcb):
                pcbExpropiado = runningPCB
                pcbExpropiado.state = PCBState.READY
                self._dispatcher.save(pcbExpropiado)
                self._scheduler.add(pcbExpropiado)
                pcb.state = PCBState.RUNNING
                self._dispatcher.load(pcb)
                self._pcbTable.runningPCB = pcb
            else:
                pcb.state = PCBState.READY
                self._scheduler.add(pcb)
        else:
            pcb.state = PCBState.RUNNING
            self._dispatcher.load(pcb)
            self._pcbTable.setRunningPCB(pcb)
