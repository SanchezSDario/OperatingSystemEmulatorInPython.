import datetime

from Classes.IRQ import InterruptionParameters
from pip import logger


class InterruptionHandlerPageFault:
    def __init__(self, kernel):
        self._pcbTable = kernel.pcbTable
        self._scheduler = kernel.scheduler
        self._dispatcher = kernel.dispatcher
        self._loader = kernel.loader

    def execute(self, irq):

        pageNumber = irq.get(InterruptionParameters.PAGE_FAULT)
        pcb = self.pcbTable.runningPCB
        pcbPid = self.pcbTable.runningPCB.pid
        
        logger.info("\nPAGE FAULT -process: {pro}, page : {p}".format
                    (pro= pcb.path,p= pageNumber))
        logger.info(self)

        frame = self.loader.memoryManager.getFreeFrame()
        pageToLoad = self.loader.hardDisk.read(pcb.path)[pageNumber]
        self.cargarYactualizar(pageToLoad, pageNumber, frame, pcbPid)
        logger.info("\nFrameSelected: {frame}".format(frame= frame))



    def cargarYactualizar(self, pageToLoad, pageNumber, frame, pid):

        tableBD = self.loader.memoryManager.pageTable.getPageTable(pid)
        row = tableBD.getRow(pageNumber)
        row.frame = frame
        row.validB = 1
        row.timeStamp =  datetime.datetime.now().timestamp()
        row.usos += 1
        tableBD.putRow(pageNumber, row)
        self.loader.memoryManager.pageTable.updatePage(pid, tableBD)

        self.loader.loadPage(pageToLoad, frame)

        self.dispatcher.mmu.pageTable = tableBD

    @property
    def pcbTable(self):
        return self._pcbTable

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def loader(self):
        return self._loader


    def __repr__(self):
        return "\nfreeFrames : {frames}".format(
            frames = self.loader.memoryManager.freeFrames)