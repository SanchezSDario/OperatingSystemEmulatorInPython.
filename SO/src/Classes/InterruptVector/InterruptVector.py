import threading

from pip import logger


class InterruptVector:

    def __init__(self):
        self._handlers = {}
        self.kernelModeLock = threading.Lock()

    def register(self, irq_type, handler):
        self.handlers[irq_type] = handler

    def handle(self, irq):
        handler = self.handlers[irq.type]
        logger.info("Handle: {handle}".format(handle= irq._type))
        self.kernelModeLock.acquire()
        handler.execute(irq)
        self.kernelModeLock.release()

    @property
    def handlers(self):
        return self._handlers