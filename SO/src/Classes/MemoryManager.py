from collections import deque

from Classes.PageTable import PageTableGlobal
from builtins import int
from _operator import index
from pip import logger
from tabulate import tabulate

from src.Classes.PageTable import PageTableRow
from src.Classes.SwapMemory import SwapMemory

class MemoryManager:
    def __init__(self, frameSize, memory):
        self._freeFrames = []
        frames = memory.size // frameSize
        for i in range(0, frames):
            self._freeFrames.append(i)

        self._frameSize = frameSize
        self._pageTable = PageTableGlobal()
        self._memory = memory

        self._waiting = []
        self._algoritmo = None


        # para cuando se hace paginacion comun (y no se si BD tambien)
        # cuando un programa no entra en memoria se lo deja esperando, como mostro en el ultimo video
        #self._swapMem = SwapMemory(16)
        #self._swapFreeFrames = []
        #for i in range(0, self._swapMem.size):
        #    self._swapFreeFrames.append(i)

    @property
    def frameSize(self):
        return self._frameSize

    @property
    def frames(self):
        return self._memory.size / self.frameSize

    @property
    def freeFrames(self):
        return self._freeFrames

    def addFreeFrame(self, frame):
        self.freeFrames.append(frame)

    @property
    def size(self):
        return self._memory.size

    @property
    def pageTable(self):
        return self._pageTable

    @property
    def memory(self):
        return self._memory

    @property
    def swap(self):
        return self._swapMem

    @property
    def algoritmo(self):
        return self._algoritmo

    @algoritmo.setter
    def algoritmo(self, x):
        self._algoritmo = x

    def getFreeFrames(self, amount):
        freeFrames = []
        for i in range(0, amount):
            freeFrame = self.freeFrames.pop()  # para cada pagina dame el primer frame que tengas
            freeFrames.append(freeFrame)
            logger.info(self)
        return freeFrames

    @property
    def swapFreeFrames(self):
        return self._swapFreeFrames

    def getSwapFreeFrame(self):
        return self._swapFreeFrames.pop()


    def getFreeFrame(self):
        if len(self.freeFrames) == 0:
            logger.info("--Algoritmo de seleccion de victima--")
            victima = self._algoritmo.selectV()
            return self.blanquearPagina(victima[0], victima[1])
        #logger.info(self)
        return self.freeFrames.pop(0)


    def blanquearPagina(self, pid, pageId):
        #swap = self.loader.memoryManager.swap
        pageT = self.pageTable.getPageTable(pid)
        pageRow = pageT.getRow(pageId)
        frame = pageRow.frame
        pageRow = PageTableRow()

       # if row.dirty == 1:
        #    if self.loader.memoryManager.swapFreeFrames > 0:
         #       swapID = self.loader.memoryManager.getSwapFreeFrame()
          #      self.loader.memoryManager.swap.put(row, swapID)
           #     row.swap = swapID

        pageT.table[pageId] = pageRow
        self.pageTable.updatePage(pid, pageT)
        logger.info("\n--Frame victim-- :{v}\n".format(v= frame))

        return frame

    def devolverFrames(self, pid):
        pageT = self.pageTable.getPageTable(pid)
        for pageNumber in range(0, len(pageT.table)):
            if pageT.table[pageNumber].validB == 1:
                self.freeFrames.append(pageT.table[pageNumber].frame)
            pageRow = PageTableRow()
            pageT.table[pageNumber] = pageRow
        self.pageTable.updatePage(pid, pageT)
        logger.info("\nFrames after KILL:\n{r}\n".format(r= self.freeFrames))


    def __repr__(self):
        return tabulate(enumerate(self._freeFrames), tablefmt='psql')
