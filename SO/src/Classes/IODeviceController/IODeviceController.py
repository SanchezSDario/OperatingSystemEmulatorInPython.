

class IODeviceController:
    def __init__(self):
        self._ids = 0
        self._deviceMap = {}

    def tickDevice(self, deviceName):
        device = self.deviceMap[deviceName]
        if device.isIdle():
            device.load_from_waiting_queue()
        else:
            device.keep_running()

    def addDevice(self, device):
        self.deviceMap[device.name] = device

    def get(self, deviceName):
        return self.deviceMap[deviceName]

    @property
    def deviceMap(self):
        return self._deviceMap

    def devices(self):
        deviceList = []
        for i in self._deviceMap:
            deviceList.append(self._deviceMap[i])
        return deviceList