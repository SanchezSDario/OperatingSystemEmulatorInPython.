from collections import deque

from Classes.IRQ import IRQ, InterruptionTypes

from pip import logger

from Classes.IRQ import InterruptionParameters
from . import IODeviceController


class IODevice:

    def __init__(self,ioDeviceController, name, runTime, iv):
        self._name = name
        self._controller = ioDeviceController
        self._runTime = runTime
        self._tickCount = 0
        self._runningPCB = None
        self._waitingQ = deque([])
        self._iv = iv

    def isIdle(self):
        return self._runningPCB is None

    def keep_running(self):
        self._tickCount += 1
        if self._runTime <= self._tickCount:
            irq = IRQ(InterruptionTypes.IOOUT)
            irq.set(InterruptionParameters.IO_OUT_PCB, self.getCurrentPcb())
            logger.info("DeviceFinish: {handle}".format(handle=self.name))
            self._iv.handle(irq)
            self.load_from_waiting_queue()

    def load_from_waiting_queue(self):
        if len(self._waitingQ) != 0:
            self.load(self._waitingQ.popleft())
            self._tickCount = 0

    def tick(self):
        self._controller.tickDevice(self.name)

    def load(self,pcb):
        self._runningPCB = pcb

    def getCurrentPcb(self):
        pcb = self._runningPCB
        self._runningPCB = None
        return pcb

    @property
    def name(self):
        return self._name

    @property
    def waitingQ(self):
        return self._waitingQ