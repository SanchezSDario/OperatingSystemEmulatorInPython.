from pip import logger

from Classes.IRQ import IRQ, InterruptionTypes, InterruptionParameters


class MMU:
    def __init__(self, mem):
        self._memory = mem
        self._baseDir = 0

    def fetch(self, pc):
        pDir = self._baseDir + pc
        return self._memory.fetch(pDir)

    @property
    def baseDir(self):
        return self._baseDir

    @baseDir.setter
    def baseDir(self, baseDir):
        self._baseDir = baseDir

    @property
    def memory(self):
        return self._memory


class MMUPaging:
    def __init__(self, memory, frameSize):
        self._memory = memory
        self._frameSize = frameSize
        self._pageTable = None

    def fetch(self, pc):
        offset = pc % self.frameSize
        page = pc // self.frameSize
        frame = self.pageTable.table[page].frame
        pcDir = frame * self.frameSize + offset
        logger.info("-fromDir: {dir} ".format(dir=pcDir))
        return self._memory.fetch(pcDir)

    @property
    def memory(self):
        return self._memory
    
    @property
    def frameSize(self):
        return self._frameSize
    
    @property
    def pageTable(self):
        return self._pageTable
    
    @pageTable.setter
    def pageTable(self, pageT):
        self._pageTable = pageT


class MMUPagingBD:
    def __init__(self, memory, frameSize):
        self._memory = memory
        self._frameSize = frameSize
        self._pageTable = None
        self._memoryManager = None
        self._kernel = None

    def fetch(self, pc):

        offset = pc % self.memoryManager.frameSize
        pageNumber = pc // self.memoryManager.frameSize
        frame = self.pageTable.table[pageNumber].frame
        if (frame == None):
            irq = IRQ(InterruptionTypes.PAGEFAULT)
            irq.set(InterruptionParameters.PAGE_FAULT, pageNumber)
            self._kernel.interruptVector.handle(irq)

            frame = self.pageTable.table[pageNumber].frame
            pcDir = frame * self.memoryManager.frameSize + offset
            return self._memory.fetch(pcDir)
        else:
            pcDir = frame * self.memoryManager.frameSize + offset
            return self._memory.fetch(pcDir)

    @property
    def memory(self):
        return self._memory

    @property
    def frameSize(self):
        return self._frameSize

    @property
    def pageTable(self):
        return self._pageTable

    @property
    def memoryManager(self):
        return self._memoryManager

    @property
    def kernel(self):
        return self._kernel

    @pageTable.setter
    def pageTable(self, pageT):
        self._pageTable = pageT

    @memoryManager.setter
    def memoryManager(self, manager):
        self._memoryManager = manager

    @kernel.setter
    def kernel(self, ker):
        self._kernel = ker
