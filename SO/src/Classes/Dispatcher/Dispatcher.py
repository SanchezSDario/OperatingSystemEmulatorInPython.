from pip import logger

from src.Classes.PCB import PCBState


class Dispatcher:
    def __init__(self, cpu, mmu, timer=None):
        self._cpu = cpu
        self._mmu = mmu
        self._timer = timer

    def isIdle(self):
        return self.cpu.isIdle()

    def load(self, pcb):
        self.cpu.pc = pcb.pc
        self.mmu.baseDir = pcb.baseDir
        self._timer.reset()
        logger.info("\n --Dispatcher-- \n Load : {path}\n".format(
            path = pcb.path))
        
    def save(self, pcb):
        pcb.setPc(self.cpu.pc)
        self.cpu.pc = -1

    @property
    def cpu(self):
        return self._cpu

    @property
    def mmu(self):
        return self._mmu

    @property
    def timer(self):
        return self._timer

    @timer.setter
    def timer(self, timerV):
        self._timer = timerV


class DispatcherPaging:
    def __init__(self, cpu, mmu, memoryManager, timer=None):
        self._cpu = cpu
        self._mmu = mmu
        self._timer = timer
        self._memoryManager = memoryManager

    def isIdle(self):
        return self.cpu.isIdle()

    def load(self, pcb):
        pageTable = self._memoryManager.pageTable.getPageTable(pcb.pid)
        self.cpu.pc = pcb.pc
        self.mmu.pageTable = pageTable
        self._timer.reset()
        logger.info("\n --Dispatcher Paging--\nLoaded: {path}".format(path=pcb._path))

    def save(self, pcb):
        pcb.setPc(self.cpu.pc)
        logger.info("\n--Dispatcher Paging--\nSaved: {path}, pcbState: {state}".format(
            path=pcb.path, state=pcb.state))
        # si el proceso termina, devuelvo los frames
        if pcb.state == PCBState.TERMINATED:
            self._memoryManager.devolverFrames(pcb.pid)
        self.cpu.pc = -1

    @property
    def cpu(self):
        return self._cpu

    @property
    def mmu(self):
        return self._mmu

    @property
    def timer(self):
        return self._timer

    @property
    def memoryManager(self):
        return self._memoryManager

    @timer.setter
    def timer(self, timerV):
        self._timer = timerV
