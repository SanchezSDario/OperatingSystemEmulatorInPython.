from pip import logger

from Classes.IRQ import InterruptionTypes

from Classes.IRQ import IRQ

from Classes.IRQ import InterruptionParameters


class Cpu:
    def __init__(self, mmu, iv=None):
        self._mmu = mmu
        self._pc = -1
        self._ir = None
        self._interruptVector = iv

    def tick(self):
        if (self.isIdle()):
            self._fetch()
            self._decode()
            self._execute()

    def _fetch(self):
        logger.info("----Fetch-pc: {x}".format(x=self._pc))
        self._ir = self._mmu.fetch(self._pc)
        self._pc += 1

    def _decode(self):
        # el decode no hace nada en este emulador
        pass

    def _execute(self):
        logger.info("----Exec: {op}, PC={pc}".format(op=self._ir, pc=self._pc))
        if (self._ir.isExit()):
            irq = IRQ(InterruptionTypes.KILL)
            self.interruptVector.handle(irq)
        elif (self._ir.isIo()):
            irq = IRQ(InterruptionTypes.IOIN)
            irq.set(InterruptionParameters.IO_DEVICE, self._ir.count)
            self.interruptVector.handle(irq)

    def getIr(self):
        return self._ir

    @property
    def pc(self):
        return self._pc

    @pc.setter
    def pc(self, addr):
        self._pc = addr

    def __repr__(self):
        return "{memoria}".format(memoria=self._mmu.memory)

    def kernel(self):
        return self._kernel

    def kernelSetter(self, kernel):
        self._kernel = kernel

    @property
    def interruptVector(self):
        return self._interruptVector

    def isIdle(self):
        return self.pc > -1

    @interruptVector.setter
    def interruptVector(self, ivV):
        self._interruptVector = ivV
