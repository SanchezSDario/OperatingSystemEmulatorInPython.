from src.Directorio import Directorio


class Command():
    def __init__(self, directorio):
        self._directorio = directorio

    def parseComando(self, string, int):
        comando = ""
        if int <= len(string):
            for i in range(0, int):
                comando = comando + string[i]
        return comando

    def parseFolder(self, string, int):
        comando = ""
        for parse in range(int + 1, len(string)):
            comando = comando + string[parse]
        return comando

    def parseDir(self, string, int):
        comando = ""
        for parse in range(int + 1, len(string)):
            if string[parse] == " ":
                break
            else:
                comando = comando + string[parse]
        return comando




class Ls(Command):
    def ejecutar(self, comando, disk):
        if "ls" == comando:
            self._directorio.verProgramasEnLaCarpetaActual()


class Home(Command):
    def ejecutar(self, comando, disk):
        if "home" == comando:
            self._directorio.Home()


class Cd(Command):
    def ejecutar(self, comando, disk):
        if "cd" == self.parseComando(comando,2):
            self._directorio.entrarEnCarpeta(self.parseFolder(comando, 2))


class Exec(Command):
    def ejecutar(self, comando, disk):
        if "exec" == self.parseComando(comando, 4):
            self._directorio.EjecutarPrograma(self.parseFolder(comando, 4))


class Create(Command):
    def ejecutar(self, comando, disk):
        if "create" == self.parseComando(comando, 6):
            self._directorio.crearArchivo(self.parseFolder(comando, 6))


class Install(Command):
    def ejecutar(self, comando, disk):
        if "install" == self.parseComando(comando, 7):
            self._directorio.crearEjecutable(self.parseFolder(comando, 6),disk)
            pass
class Exit(Command):
    def ejecutar(self,comando,disk):
        if "exit" == comando:
            print("")
    def exit(self,comando):
        if "exit" == comando:
            return False
        else:
            return True

