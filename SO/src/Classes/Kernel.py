from pip import logger

import sys
sys.path.append('../')

from Classes.IRQ import IRQ, InterruptionTypes, InterruptionParameters
from Classes.PCBTable import PCBTable


class Kernel:
    def __init__(self, dispatcher=None, scheduler=None, loader=None, interruptVector=None, ioDeviceController=None):
        self._dispatcher = dispatcher
        self._scheduler = scheduler
        self._pcbTable = PCBTable()
        self._loader = loader
        self._interruptVector=interruptVector
        self._ioDeviceController = ioDeviceController

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def pcbTable(self):
        return self._pcbTable

    @property
    def loader(self):
        return self._loader

    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def interruptVector(self):
        return self._interruptVector

    @dispatcher.setter
    def dispatcher(self, dispatcherV):
        self._dispatcher = dispatcherV

    @loader.setter
    def loader(self, loaderV):
        self._loader = loaderV

    @scheduler.setter
    def scheduler(self, schedulerV):
        self._scheduler = schedulerV

    @interruptVector.setter
    def interruptVector(self, interruptV):
        self._interruptVector = interruptV

    @ioDeviceController.setter
    def ioDeviceController(self, idcV):
        self._ioDeviceController = idcV

    def exec(self, programName, priority=0):
        irq = IRQ(InterruptionTypes.NEW)
        irq.set(InterruptionParameters.NEW_PROGRAM_NAME, programName)
        irq.set(InterruptionParameters.PCB_PRIORITY, priority)
        self._interruptVector.handle(irq)