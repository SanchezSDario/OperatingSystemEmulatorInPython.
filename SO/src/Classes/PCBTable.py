from Classes.PCB import PCB

class PCBTable:
    def __init__(self, table = {}):
        self._table = table
        self._currentPid = 0
        self._runningPCB = None

    @property
    def runningPCB(self):
        return  self._runningPCB

    @runningPCB.setter
    def runningPCB(self, pcb):
        self._runningPCB = pcb

    def get(self, pid):
        return self._table.get(pid)

    def add(self, Pcb):
        self._table[self._currentPid] = Pcb

    def getNewPID(self):
        current = self._currentPid
        self._currentPid += 1        
        return current
        

    def remove (self, pid):
        del self._table[pid]

    def setRunningPCB(self,pcb):
        self._runningPCB = pcb

    def __repr__(self):
        return "\n--PCBTable-- \n{pcbs} \n-- --\n".format(
            pcbs = self._table)
