class Scheduler:

    def __init__(self, scheduling):
        self._scheduling = scheduling

    def add(self, pcb):
        self._scheduling.add(pcb)

    def getNext(self):
        return self._scheduling.getNext()

    def isEmpty(self):
        return self._scheduling.isEmpty()