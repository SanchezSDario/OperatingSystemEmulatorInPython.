from tabulate import tabulate

class PriorityExp:


    def __init__(self, prio):
        self._readyQ = []
        self._maxprio = prio
        for i in range(0, self._maxprio):
            self._readyQ.append([])
        self._waitingQ = []

    def add(self, pcb):
        self._readyQ[pcb.priority].append(pcb)

    def getNext(self):
        for i in range(0, self._maxprio):
            if not self.isEmptyList(self._readyQ[i]):
                return self._readyQ[i].pop()
        
        return None

    def isEmptyList(self, i):
       return  len(i) == 0   
        
    def isEmpty(self):
        for i in range(0 , self._maxprio):
            if not self.isEmptyList(self._readyQ[i]):
                return False
        
        return True

    def mustExpropiate(self,pcbInCPU,pcbToAdd):
        return(pcbToAdd.priority < pcbInCPU.priority)

    @property
    def waitingQ(self):
        return self._waitingQ
    
    def addToWaitingQ(self, pcb):
        self._waitingQ.append(pcb)
        
    def getFromWaitingQ(self):
        return self._waitingQ.pop(0)
    
    def __repr__(self):
        return tabulate(enumerate(self._readyQ), tablefmt = 'psql')