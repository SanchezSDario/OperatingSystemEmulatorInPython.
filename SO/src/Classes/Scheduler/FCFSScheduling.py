from tabulate import tabulate

class FCFSScheduling:

    def __init__(self):
        self._readyQ = []
        self._waitingQ = []

    def add(self, pcb):
        self._readyQ.append(pcb)

    def getNext(self):
        return self._readyQ.pop(0)

    def isEmpty(self):
        return len(self._readyQ) == 0
    
    def mustExpropiate(self, pcbInCPU, pcbToAdd):
        return False
    
    @property
    def waitingQ(self):
        return self._waitingQ
    
    def addToWaitingQ(self, pcb):
        self._waitingQ.append(pcb)
        
    def getFromWaitingQ(self):
        return self._waitingQ.pop(0)
    
    def __repr__(self):
        return tabulate(enumerate(self._readyQ), tablefmt = 'psql')