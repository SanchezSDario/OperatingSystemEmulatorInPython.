from time import sleep


class Clock:
    def __init__(self, cpu=None, ioDeviceList=None, timer=None):
        self._cpu = cpu
        self._ioDeviceĹist = ioDeviceList
        self._timer = timer

    def start(self):
        while True:
            self.ticks()
            sleep(1)

    def ticks(self, quantity=1):
        for i in range(0, quantity):
            self._timer.tick()
            self.tickIODeviceList()
            self._cpu.tick()
            sleep(0.5)

    def tickIODeviceList(self):
        for device in self._ioDeviceĹist:
            device.tick()

    @property
    def cpu(self):
        return self._cpu

    @property
    def ioDeviceList(self):
        return self._ioDeviceĹist

    @property
    def timer(self):
        return self._timer

    @cpu.setter
    def cpu(self, cpuV):
        self._cpu = cpuV

    @ioDeviceList.setter
    def ioDeviceList(self, idV):
        self._ioDeviceĹist = idV

    @timer.setter
    def timer(self, timerV):
        self._timer = timerV
