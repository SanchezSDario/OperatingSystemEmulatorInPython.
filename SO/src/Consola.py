from Classes.Instructions.CPU import CPU
from Classes.Instructions.IO import IO
from Classes.Kernel import Kernel
from Classes.HardDisk import HardDisk
from Classes import Clock
from Classes.Directorio.Executable import Executable
from Classes.Directorio.Folder import Folder
from Classes.Command.Commands import Command, Ls, Home, Exec, Create, Install,Cd,Exit
from pip import logger

class Consola():

    def __init__(self, directorio,disk, kernel):
        self._kernel = kernel
        self._directorio = directorio
        self._disk = disk
        self.commands = [Exit(directorio), Ls(directorio),Home(directorio), Cd(directorio), Exec(directorio), Create(directorio), Install(directorio)]


    def esComandoMemory(self,comando):
        if comando == "memory":
            logger.info(self)

    def run(self):
        running = True

        while (running):
            comando = input()
            for command in self.commands:
                command.ejecutar(comando,self._disk)
            self.esComandoMemory(comando)

            running = self.commands[0].exit(comando)




    def __repr__(self):
        return "{memoria}".format(memoria=self._kernel.loader.memory)

