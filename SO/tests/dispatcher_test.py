import unittest

import sys
sys.path.append('../src')

from src.Classes.Dispatcher.Cpu import Cpu
from src.Classes.Dispatcher.MMU import MMU
from src.Classes.DummyTimer import DummyTimer
from src.Classes.Instructions.CPU import CPU
from src.Classes.Instructions.IO import IO
from src.Classes.InterruptVector.InterruptVector import InterruptVector
from src.Classes.Memory import Memory
from src.Classes.PCB import PCB, PCBState
from src.Classes.Dispatcher.Dispatcher import Dispatcher

class testDispatcherMethods(unittest.TestCase):

    def setUp(self):
        self._memory = Memory(32)
        self._mmu = MMU(self._memory)
        self._iv = InterruptVector()
        self._cpu = Cpu(self._mmu, self._iv)
        self._pcb = PCB(1, 0, "pcb1", PCBState.READY)
        self._dispatcher = Dispatcher(self._cpu, self._mmu, DummyTimer())

    def test_load(self):
        self._pcb.setPc(5)
        self._pcb.baseDir = 5
        self._dispatcher.load(self._pcb)
        self.assertEqual(5, self._cpu.pc)
        self.assertEqual(5, self._mmu.baseDir)

    def test_save(self):
        self._cpu.pc = 7
        self._dispatcher.save(self._pcb)
        self.assertEqual(7, self._pcb.pc)
        self.assertEqual(-1, self._cpu.pc)

if __name__ == '__main__':
    unittest.main()
