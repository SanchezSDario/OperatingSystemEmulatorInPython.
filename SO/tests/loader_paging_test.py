import unittest

import sys

sys.path.append('../src')

from src.Classes.Instructions.CPU import CPU
from src.Classes.PCB import PCBState, PCB
from src.Factory import Factory


class testLoaderPagingMethods(unittest.TestCase):
    def setUp(self):
        self._factory = Factory()
        self._memCapacity = 10
        self._frameSize = 2
        self._factory.withMemoryPaging(self._memCapacity, self._frameSize)
        self._factory.withSchedulingFCFS()
        self._factory.withDefaultInterruptions()
        self._disk = self._factory.disk
        self._loader = self._factory.kernel._loader
        self._memoryManager = self._loader.memoryManager

    def test_load(self):
        print("------------------------------------------------------------")
        print("Test load")
        print("")

        instructions = [CPU(7)]
        pcb = PCB(0, 0, "programa1", PCBState.READY)
        self._disk.write("programa1", instructions, self._frameSize)

        print("Memoria Previa a la carga:\n{memo}".format(memo = self._memoryManager.memory))
        print("")

        self._loader.load(pcb)
        self.assertEqual(1, len(self._memoryManager.freeFrames))

        print("Frame libres: {r}".format(r= self._memoryManager.freeFrames))
        print("")
        print("Memoria despues de la carga:\n{mem}".format(mem= self._memoryManager.memory))
        print("")

        self._memoryManager.devolverFrames(pcb.pid)

        print("Frames libres si entra un nuevo proceso: {framesLibresFuturos}"
              .format(framesLibresFuturos= self._memoryManager.freeFrames))
        print("")

    def test_load_page(self):
        print("------------------------------------------------------------")
        print("Test load page")
        print("")

        print("Frame libres: {r}".format(r=self._memoryManager.freeFrames))
        print("")
        print("Memoria previa a la carga:\n{mem}".format(mem=self._memoryManager.memory))
        print("")

        instructions = [CPU(3)]
        pcb = PCB(0, 0, "programa1", PCBState.READY)
        self._disk.write("programa1", instructions, self._frameSize)
        pages = self._disk.read(pcb.path)
        frame = self._memoryManager.getFreeFrame()
        self._loader.loadPage(pages[0], frame)

        print("Frame libres: {r}".format(r=self._memoryManager.freeFrames))
        print("")
        print("Memoria despues de la carga del frame 1:\n{mem}".format(mem=self._memoryManager.memory))
        print("")

        frame = self._memoryManager.getFreeFrame()
        self._loader.loadPage(pages[1], frame)

        print("Frame libres: {r}".format(r=self._memoryManager.freeFrames))
        print("")
        print("Memoria despues de la carga del frame 2:\n{mem}".format(mem=self._memoryManager.memory))
        print("")