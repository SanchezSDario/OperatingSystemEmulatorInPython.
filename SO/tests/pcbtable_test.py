import unittest

import sys
sys.path.append('../src')


from src.Classes.PCB import PCB, PCBState
from src.Classes.PCBTable import PCBTable


class testPcbTableMethods(unittest.TestCase):
    def setUp(self):
        self._pcb1 = PCB(5, 0, "path1", PCBState.READY)
        self._pcb2 = PCB(10, 5, "path2", PCBState.READY)
        self._pcbTable = PCBTable()
        self._pcbTable.add(self._pcb1)
        self._pcbTable.getNewPID()
        self._pcbTable.add(self._pcb2)

    def test_get(self):
        self._pcb = self._pcbTable.get(0)
        self.assertEqual(self._pcb.pid, 5)
        self._pcb = self._pcbTable.get(1)
        self.assertEqual(self._pcb.pid, 10)
