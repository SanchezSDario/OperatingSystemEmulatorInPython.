import unittest

import sys
sys.path.append('../src')

from src.Classes.PCB import PCB, PCBState
from src.Classes.Scheduler.FCFSScheduling import FCFSScheduling


class testFCFSMethods(unittest.TestCase):
    def setUp(self):
        self._pcb0 = PCB(0, 0, "program1", PCBState.NEW)
        self._pcb1 = PCB(1, 1, "program2", PCBState.NEW)
        self._pcb2 = PCB(2, 2, "program3", PCBState.NEW)
        self._scheduling = FCFSScheduling()

    def test_isEmpty(self):
        self.assertTrue(self._scheduling.isEmpty())

    def test_add(self):
        self._scheduling.add(self._pcb0)
        self._scheduling.add(self._pcb1)
        self._scheduling.add(self._pcb2)
        self.assertFalse(self._scheduling.isEmpty())
        self.assertEqual(len(self._scheduling._readyQ), 3)

    def test_getNext(self):
        self._scheduling.add(self._pcb1)
        self.assertEqual(self._scheduling.getNext(), self._pcb1)