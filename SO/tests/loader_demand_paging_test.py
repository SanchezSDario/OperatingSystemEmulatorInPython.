import unittest

import sys

from src.Classes.LRU import LRU

sys.path.append('../src')

from src.Classes.Instructions.CPU import CPU
from src.Classes.PCB import PCBState, PCB
from src.Factory import Factory


class testLoaderPagingMethods(unittest.TestCase):
    def setUp(self):
        self._factory = Factory()
        self._algorithm = LRU()
        self._memCapacity = 10
        self._frameSize = 2
        self._factory.withMemoryDemandPaging(self._memCapacity, self._frameSize, self._algorithm)
        self._factory.withSchedulingFCFS()
        self._factory.withDefaultInterruptions()
        self._disk = self._factory.disk
        self._loader = self._factory.kernel._loader
        self._memoryManager = self._loader.memoryManager

    def test_load(self):
        print("------------------------------------------------------------")
        print("Test load")
        print("")

        instructions = [CPU(7)]
        pcb = PCB(0, 0, "programa1", PCBState.READY)
        self._disk.write("programa1", instructions, self._frameSize)

        # print("Page table local del proceso antes de la carga:\n{memo}".format(memo = self._memoryManager.pageTable.table[pcb.pid]))
        # print("")

        self._loader.load(pcb)
        self.assertEqual(5, len(self._memoryManager.freeFrames))

        print("Page table local del proceso despues de instanciar las paginas vacias en ella:\n{mem}"
              .format(mem= self._memoryManager.pageTable.table[pcb.pid]))
        print("")

    def test_load_page(self):
        print("------------------------------------------------------------")
        print("Test load page")
        print("")

        instructions = [CPU(3)]
        pcb = PCB(0, 0, "programa1", PCBState.READY)
        self._disk.write("programa1", instructions, self._frameSize)
        pages = self._disk.read(pcb.path)
        frame = self._memoryManager.getFreeFrame()
        self._loader.load(pcb)
        self._loader.loadPage(pages[0], frame)

        print("Rows de la page table local del proceso antes de la carga:\n{mem}"
              .format(mem=self._memoryManager.pageTable.table[pcb.pid]))
        print("")

        pageNumber = 0
        pageToLoad = self._loader.hardDisk.read(pcb.path)[pageNumber]
        frame1 = self._memoryManager.getFreeFrame()
        tableBD = self._loader.memoryManager.pageTable.getPageTable(pcb.pid)

        row = tableBD.getRow(pageNumber)
        row.frame = frame1
        row.validB = 1
        row.timeStamp = None
        row.usos += 1
        tableBD.putRow(pageNumber, row)

        frame2 = self._memoryManager.getFreeFrame()
        pageNumber += 1
        row = tableBD.getRow(pageNumber)
        row.frame = frame2
        row.validB = 1
        row.timeStamp = None
        row.usos += 1
        tableBD.putRow(pageNumber, row)

        self._loader.memoryManager.pageTable.updatePage(pcb.pid, tableBD)
        self._loader.loadPage(pageToLoad, frame)

        print("Rows de la page table local del proceso despues de la carga:\n{mem}"
              .format(mem=self._memoryManager.pageTable.table[pcb.pid]))
        print("")