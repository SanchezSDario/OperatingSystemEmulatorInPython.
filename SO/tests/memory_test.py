import unittest

import sys
sys.path.append('../src')

from src.Classes.Instructions.CPU import CPU
from src.Classes.Instructions.IO import IO
from src.Classes.Memory import Memory

class testMemoryMethods(unittest.TestCase):

    def setUp(self):
        self._memory = Memory(10)

    def test_put_fetch(self):

        self._memory.put(0, CPU())
        self._memory.put(1, CPU())
        self._memory.put(2, CPU())
        self._memory.put(3, IO("device"))
        self._memory.put(4, IO("device"))
        self._memory.put(5, CPU())

        print(self._memory)
        self.assertTrue(self._memory.fetch(0).isCpu())
        self.assertTrue(self._memory.fetch(1).isCpu())
        self.assertTrue(self._memory.fetch(2).isCpu())
        self.assertTrue(self._memory.fetch(3).isIo())
        self.assertTrue(self._memory.fetch(4).isIo())
        self.assertTrue(self._memory.fetch(5).isCpu())

if __name__ == '__main__':
    unittest.main()