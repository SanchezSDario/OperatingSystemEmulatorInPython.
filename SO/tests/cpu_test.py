import unittest

import sys
sys.path.append('../src')

from src.Classes.Dispatcher.Cpu import Cpu
from src.Classes.Dispatcher.MMU import MMU
from src.Classes.Instructions.CPU import CPU
from src.Classes.Instructions.EXIT import EXIT
from src.Classes.Instructions.IO import IO
from src.Classes.InterruptVector.InterruptVector import InterruptVector
from src.Classes.Memory import Memory


class testCpuMethods(unittest.TestCase):

    def setUp(self):
        self._iv = InterruptVector()
        self._memory = Memory(32)
        self._mmu = MMU(self._memory)
        self._cpu = Cpu(self._mmu, self._iv)
        self._cpu.pc = 0
        self._memory.put(0, CPU())
        self._memory.put(1, IO("device"))
        self._memory.put(2, EXIT())

    def test_fetch(self):
        self._cpu._fetch()
        self.assertEquals(1, self._cpu.pc)
        self.assertTrue(self._cpu.getIr().isCpu())
        self._cpu._fetch()
        self.assertEquals(2, self._cpu.pc)
        self.assertTrue(self._cpu.getIr().isIo())

if __name__ == '__main__':
    unittest.main()
