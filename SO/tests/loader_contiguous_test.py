import unittest

import sys
sys.path.append('../src')

from src.Classes.HardDisk import HardDisk
from src.Classes.Instructions.CPU import CPU
from src.Classes.Instructions.IO import IO
from src.Classes.Loader import Loader
from src.Classes.Memory import Memory
from src.Classes.PCB import PCB, PCBState


class testLoaderMethods(unittest.TestCase):
    def setUp(self):
        self._hardDisk = HardDisk()
        self._memory = Memory(32)
        self._loader = Loader(self._hardDisk, self._memory)
        self._pcb = PCB(0, 0, "program1", PCBState.NEW)
        self._instructions = [CPU(2), IO("device"), CPU(1)]

    def test_load(self):
        self._hardDisk.write("program1", self._instructions)
        self._pcb.baseDir = 5
        self._loader.load(self._pcb)
        self.assertEqual(self._pcb.baseDir, 0)
        self.assertTrue(self._memory.get(0).isCpu())
        self.assertTrue(self._memory.get(1).isCpu())
        self.assertTrue(self._memory.get(2).isIo())
        self.assertTrue(self._memory.get(3).isCpu())
