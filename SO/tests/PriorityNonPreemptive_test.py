import unittest

import sys
sys.path.append('../src')


from src.Classes.PCB import PCB, PCBState
from src.Classes.Scheduler.PriorityNoExp import PriorityNoExp


class testPriorityNonPreemptiveMethods(unittest.TestCase):
    def setUp(self):
        self._pcb0 = PCB(0, 0, "program1", PCBState.READY)
        self._pcb1 = PCB(1, 1, "program2", PCBState.READY)
        self._pcb2 = PCB(2, 2, "program3", PCBState.READY)
        self._pcb0.priority = 1
        self._pcb1.priority = 3
        self._pcb2.priority = 4
        self._scheduling = PriorityNoExp(5)

    def test_isEmpty(self):
        self.assertTrue(self._scheduling.isEmpty())

    def test_add(self):
        self._scheduling.add(self._pcb2)
        self._scheduling.add(self._pcb0)
        self._scheduling.add(self._pcb1)
        self.assertFalse(self._scheduling.isEmpty())
        self.assertEqual(len(self._scheduling._readyQ[1]), 1)
        self.assertEqual(len(self._scheduling._readyQ[3]), 1)
        self.assertEqual(len(self._scheduling._readyQ[4]), 1)

    def test_getNext(self):
        self._scheduling.add(self._pcb2)
        self._scheduling.add(self._pcb0)
        self._scheduling.add(self._pcb1)
        self.assertEqual(self._scheduling.getNext(), self._pcb0)