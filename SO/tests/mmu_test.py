import unittest

import sys
sys.path.append('../src')

from src.Classes.Dispatcher.MMU import MMU
from src.Classes.Instructions.CPU import CPU
from src.Classes.Instructions.IO import IO
from src.Classes.Memory import Memory

class TestMmuMethods(unittest.TestCase):

    def setUp(self):
        self._memory = Memory(4)
        self._memory.put(0, CPU())
        self._memory.put(1, IO("Device"))
        self._mmu = MMU(self._memory)

    def test_first_program(self):
        self._mmu.baseDir = 0
        self.assertTrue(self._mmu.fetch(0).isCpu())
        self.assertTrue(self._mmu.fetch(1).isIo())
        print(self._mmu.memory.getMemory)

if __name__ == '__main__':
    unittest.main()